package com.stayclean.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.stayclean.R;
import com.stayclean.model.Category;

import java.util.List;

/**
 * Created by rioswarawan on 2/21/17.
 */

public class CategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<Category> categories;
    private LayoutInflater inflater;
    private CategoryListener categoryListener;

    public CategoryAdapter(Context context, List<Category> categories) {
        this.context = context;
        this.categories = categories;
        this.inflater = LayoutInflater.from(context);
    }

    public void setCategoryListener(CategoryListener categoryListener) {
        this.categoryListener = categoryListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_category, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Category category = categories.get(position);
        ViewHolder body = (ViewHolder) holder;
        body.populate(category, position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        private TextView title;
        private ImageView image;
        private LinearLayout content;
        private int position;

        ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            image = (ImageView) itemView.findViewById(R.id.img);
            content = (LinearLayout) itemView.findViewById(R.id.layout);
        }

        public void populate(Category model, int position) {
            this.position = position;

            title.setText(model.title);
            Picasso.with(context).load(model.img).into(image);
            content.setOnClickListener(onItemClicked);
        }

        private View.OnClickListener onItemClicked = view -> categoryListener.onSelected(position);
    }

    public interface CategoryListener {
        void onSelected(int position);
    }
}
