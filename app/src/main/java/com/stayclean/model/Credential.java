package com.stayclean.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by rioswarawan on 2/21/17.
 */

public class Credential implements Parcelable {
    public String username;
    public String password;

    public Credential() {
    }

    protected Credential(Parcel in) {
        username = in.readString();
        password = in.readString();
    }

    public static final Creator<Credential> CREATOR = new Creator<Credential>() {
        @Override
        public Credential createFromParcel(Parcel in) {
            return new Credential(in);
        }

        @Override
        public Credential[] newArray(int size) {
            return new Credential[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(username);
        parcel.writeString(password);
    }
}
