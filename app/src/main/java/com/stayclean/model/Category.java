package com.stayclean.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by rioswarawan on 2/21/17.
 */

public class Category implements Parcelable {

    public int id;
    public int img;
    public String title;

    public Category(int id, int img, String title) {
        this.id = id;
        this.img = img;
        this.title = title;
    }

    protected Category(Parcel in) {
        id = in.readInt();
        img = in.readInt();
        title = in.readString();
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(img);
        parcel.writeString(title);
    }
}
