package com.stayclean.preference;

import android.content.Context;
import android.content.SharedPreferences;

import com.stayclean.util.Constant;

/**
 * Created by rioswarawan on 1/22/17.
 */

public class PreferenceManager {

    private static SharedPreferences preferences;

    /**
     * Store string value to shared preference
     */
    private static void putString(Context context, String preferenceName, String object) {
        preferences = context.getSharedPreferences(Constant.PREFERENCE, Context.MODE_PRIVATE);
        preferences.edit().putString(preferenceName, object).apply();
    }

    /**
     * Store integer value to shared preference
     */
    private static void putInt(Context context, String preferenceName, int object) {
        preferences = context.getSharedPreferences(Constant.PREFERENCE, Context.MODE_PRIVATE);
        preferences.edit().putInt(preferenceName, object).apply();
    }

    /**
     * Store boolean value to shared preference
     */
    private static void putBool(Context context, String preferenceName, boolean object) {
        preferences = context.getSharedPreferences(Constant.PREFERENCE, 0);
        preferences.edit().putBoolean(preferenceName, object).apply();
    }

    /**
     * Return string value to shared preference
     */
    private static String getString(Context context, String preferenceName, String defaultValue) {
        preferences = context.getSharedPreferences(Constant.PREFERENCE, Context.MODE_PRIVATE);
        preferences.edit().apply();

        return preferences.getString(preferenceName, defaultValue);
    }

    /**
     * Return integer value to shared preference
     */
    private static int getInt(Context context, String preferenceName, int defaultValue) {
        preferences = context.getSharedPreferences(Constant.PREFERENCE, Context.MODE_PRIVATE);
        preferences.edit().apply();

        return preferences.getInt(preferenceName, defaultValue);
    }

    /**
     * Return boolean value to shared preference
     */
    private static boolean getBool(Context context, String preferenceName, boolean defaultValue) {
        preferences = context.getSharedPreferences(Constant.PREFERENCE, Context.MODE_PRIVATE);
        preferences.edit().apply();

        return preferences.getBoolean(preferenceName, defaultValue);
    }

    public static void clear(Context context) {
        preferences = context.getSharedPreferences(Constant.PREFERENCE, Context.MODE_PRIVATE);
        preferences.edit().clear().apply();
    }

    public static void setKeepSignedIn(Context context) {
        String key = Constant.PREFERENCE_BOOL_KEEP_SIGN_IN;
        putBool(context, key, true);
    }

    public static boolean isSignedIn(Context context) {
        String key = Constant.PREFERENCE_BOOL_KEEP_SIGN_IN;
        return getBool(context, key, false);
    }
}
