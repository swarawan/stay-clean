package com.stayclean.util;

import com.stayclean.R;

/**
 * Created by rioswarawan on 2/14/17.
 */

public class Constant {

    // =============================================
    //                  Default
    // =============================================

    public static final String DUMMY_USERNAME = "app@stayclean.com";
    public static final String DUMMY_PASSWORD = "123456";


    // =============================================
    //                  Shared Preferences
    // =============================================

    public static final String PREFERENCE = "stay-clean-prefs";
    public static final String PREFERENCE_BOOL_KEEP_SIGN_IN = "stay-clean-keep-signed";

    // =============================================
    //                  Dummy
    // =============================================
    public static final int BASE_ID = 1;
    public static final int[] DUMMY_ID = {
            BASE_ID,
            BASE_ID << 1,
            BASE_ID << 2,
            BASE_ID << 3
    };
    public static final int[] DUMMY_IMG = {
            R.drawable.ic_clothes,
            R.drawable.ic_bedcover,
            R.drawable.ic_shoes,
            R.drawable.ic_helmet
    };
    public static final String[] DUMMY_TITLE = {
            "Pakaian",
            "Sprei / Bed Cover",
            "Sepatu",
            "Helm"
    };

}
