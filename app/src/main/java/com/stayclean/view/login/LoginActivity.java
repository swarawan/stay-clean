package com.stayclean.view.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.stayclean.BuildConfig;
import com.stayclean.R;
import com.stayclean.model.Credential;
import com.stayclean.util.Constant;
import com.stayclean.view.login.presenter.LoginPresenter;
import com.stayclean.view.main.MainActivity;

public class LoginActivity extends AppCompatActivity {

    private LoginPresenter presenter;
    private Credential credential;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
    }

    private void initView() {
        presenter = new LoginPresenter(this, onLoginListener);
        credential = new Credential();

        ((EditText) findViewById(R.id.username)).setText(BuildConfig.DEBUG ? Constant.DUMMY_USERNAME : "");
        ((EditText) findViewById(R.id.password)).setText(BuildConfig.DEBUG ? Constant.DUMMY_PASSWORD : "");

        findViewById(R.id.login).setOnClickListener(onLoginClicked);
        findViewById(R.id.forgot_password).setOnClickListener(onForgotPassword);
    }

    private View.OnClickListener onLoginClicked = view -> {
        credential.username = ((EditText) findViewById(R.id.username)).getText().toString();
        credential.password = ((EditText) findViewById(R.id.password)).getText().toString();
        presenter.loginNow(credential);
    };

    private View.OnClickListener onForgotPassword = view -> presenter.forgotPassword();

    private LoginPresenter.ILoginListener onLoginListener = new LoginPresenter.ILoginListener() {
        @Override
        public void onLogin() {
            startActivity(new Intent(LoginActivity.this, MainActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
        }

        @Override
        public void onForgotPassword() {

        }

        @Override
        public void showLoading() {

        }

        @Override
        public void hideLoading() {

        }

        @Override
        public void onError(String msg) {
            if (msg != null)
                Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();
        }
    };

}
