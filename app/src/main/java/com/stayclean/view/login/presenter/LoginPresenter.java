package com.stayclean.view.login.presenter;

import android.content.Context;
import android.os.CountDownTimer;

import com.stayclean.base.BasePresenter;
import com.stayclean.base.MvpView;
import com.stayclean.model.Credential;
import com.stayclean.util.Constant;

/**
 * Created by rioswarawan on 2/16/17.
 */

public class LoginPresenter extends BasePresenter<LoginPresenter.ILoginListener> {

    private Context context;

    public LoginPresenter(Context context) {
        this.context = context;
    }

    public LoginPresenter(Context context, ILoginListener listener) {
        this.context = context;
        attachView(listener);
    }

    @Override
    public void attachView(ILoginListener mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    /**
     * Do login with username and password
     */
    public void loginNow(Credential credential) {
        checkViewAttached();
        getMvpView().showLoading();

        // do something awesome when doing login
        if (credential != null) {
            if (credential.username.equals(Constant.DUMMY_USERNAME)
                    && credential.password.equals(Constant.DUMMY_PASSWORD)) {
                getMvpView().onLogin();
            } else {
                getMvpView().onError("Username dan password tidak sesuai");
            }
        } else {
            getMvpView().onError("Username dan password tidak boleh kosong");
        }
        getMvpView().hideLoading();
    }

    /**
     * Forgot password
     */
    public void forgotPassword() {
        checkViewAttached();
        getMvpView().showLoading();

        getMvpView().onForgotPassword();

        getMvpView().hideLoading();
    }

    public interface ILoginListener extends MvpView {
        void onLogin();

        void onForgotPassword();
    }
}
