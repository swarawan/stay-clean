package com.stayclean.view.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.stayclean.R;
import com.stayclean.adapter.CategoryAdapter;
import com.stayclean.model.Category;
import com.stayclean.util.Constant;
import com.stayclean.view.main.presenter.MainPresenter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rioswarawan on 2/21/17.
 */

public class MainActivity extends AppCompatActivity {

    private CategoryAdapter categoryAdapter;
    private List<Category> data;
    private MainPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        presenter.getCategories();
    }

    private void initView() {
        data = new ArrayList<>();
        categoryAdapter = new CategoryAdapter(this, data);
        categoryAdapter.setCategoryListener(onCategoryListener);

        presenter = new MainPresenter(this);
        presenter.attachView(onMainListener);

        ((Toolbar) findViewById(R.id.toolbar)).setTitle(getString(R.string.app_name));
        ((RecyclerView) findViewById(R.id.category_list)).setLayoutManager(new GridLayoutManager(this, 2));
        ((RecyclerView) findViewById(R.id.category_list)).hasFixedSize();
        ((RecyclerView) findViewById(R.id.category_list)).setAdapter(categoryAdapter);
    }

    private CategoryAdapter.CategoryListener onCategoryListener = position -> {

    };

    private MainPresenter.IMainListener onMainListener = new MainPresenter.IMainListener() {
        @Override
        public void onCategoryFethced(List<Category> categories) {
            if (categories.size() > 0) {
                data.clear();
                data.addAll(categories);
                categoryAdapter.notifyDataSetChanged();
            }
        }

        @Override
        public void showLoading() {

        }

        @Override
        public void hideLoading() {

        }

        @Override
        public void onError(String msg) {

        }
    };
}
