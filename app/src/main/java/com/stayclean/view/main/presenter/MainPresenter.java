package com.stayclean.view.main.presenter;

import android.content.Context;
import android.os.CountDownTimer;

import com.stayclean.base.BasePresenter;
import com.stayclean.base.MvpView;
import com.stayclean.model.Category;
import com.stayclean.util.Constant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rioswarawan on 2/16/17.
 */

public class MainPresenter extends BasePresenter<MainPresenter.IMainListener> {

    private Context context;

    public MainPresenter(Context context) {
        this.context = context;
    }

    public MainPresenter(Context context, IMainListener listener) {
        this.context = context;
        attachView(listener);
    }

    @Override
    public void attachView(IMainListener mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    /**
     * Get dummy categories
     */
    public void getCategories() {
        checkViewAttached();
        getMvpView().showLoading();

        // add dummy data
        List<Category> categories = new ArrayList<>();
        for (int i = 0; i < Constant.DUMMY_ID.length; i++) {
            categories.add(new Category(
                    Constant.DUMMY_ID[i],
                    Constant.DUMMY_IMG[i],
                    Constant.DUMMY_TITLE[i]
            ));
        }

        getMvpView().onCategoryFethced(categories);
        getMvpView().hideLoading();
    }

    public interface IMainListener extends MvpView {
        void onCategoryFethced(List<Category> categories);
    }
}
