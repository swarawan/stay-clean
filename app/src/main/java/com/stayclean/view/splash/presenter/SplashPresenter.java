package com.stayclean.view.splash.presenter;

import android.content.Context;
import android.os.CountDownTimer;

import com.stayclean.base.BasePresenter;
import com.stayclean.base.MvpView;

/**
 * Created by rioswarawan on 2/16/17.
 */

public class SplashPresenter extends BasePresenter<SplashPresenter.ISplashListener> {

    private Context context;

    public SplashPresenter(Context context) {
        this.context = context;
    }

    public SplashPresenter(Context context, ISplashListener listener) {
        this.context = context;
        attachView(listener);
    }

    @Override
    public void attachView(ISplashListener mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    /**
     * Count on 3 on splash screen before the action
     */
    public void splashNow() {
        checkViewAttached();
        getMvpView().showLoading();

        CountDownTimer countDownTimer = new CountDownTimer(3000, 1000) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                getMvpView().onSplashFinished();
                getMvpView().hideLoading();
            }
        };
        countDownTimer.start();

    }

    public interface ISplashListener extends MvpView {
        void onSplashFinished();
    }
}
