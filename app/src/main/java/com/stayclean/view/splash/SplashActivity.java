package com.stayclean.view.splash;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.stayclean.R;
import com.stayclean.preference.PreferenceManager;
import com.stayclean.view.login.LoginActivity;
import com.stayclean.view.main.MainActivity;
import com.stayclean.view.splash.presenter.SplashPresenter;

public class SplashActivity extends AppCompatActivity {

    private SplashPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        initView();
        presenter.splashNow();
    }

    private void initView() {
        presenter = new SplashPresenter(this, onSplashListener);
    }

    /**
     * Check if user logged in or not
     */
    private void launchApp() {
        boolean isSignedIn = PreferenceManager.isSignedIn(this);
        Intent intent = new Intent(this, isSignedIn ? MainActivity.class : LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private SplashPresenter.ISplashListener onSplashListener = new SplashPresenter.ISplashListener() {
        @Override
        public void onSplashFinished() {
            launchApp();
        }

        @Override
        public void showLoading() {

        }

        @Override
        public void hideLoading() {

        }

        @Override
        public void onError(String msg) {

        }
    };
}
